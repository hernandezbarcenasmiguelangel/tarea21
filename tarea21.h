struct carro {
  int modelo;
  char marca[50];
  int asientos;
  char condicion[50];
};

struct computadora{
  char procesador[50];
  char marca[50];
  int RAM;
  int ROM;
};

struct alumno{
  char nombre[100];
  char sexo[50];
  int edad;
  float estatura;
};

struct equipoFutbol {
  int integrantes;
  char nombre[60];
  char director[60];
};

struct mascota {
  char nombre[100];
  char tipo[50];
  char raza [50];
  float estatura;
};

struct videoJuego {
  char nombre[100];
  float espacioMB;
  char personajeFav[100];
};

struct superHeroe {
  char nombre[100];
  char superPoder[100];
  int perSalvadas;
};

struct Planetas {
  float masa;
  char nombre[100];
  float distSol;
  float diametro;
  int lunas;
};

struct persJuego {
  char juego[100];
  char nombre[100];
  char poder[100];
};

struct comic {
  char nombre[100];
  char autor[100];
  char ilustrador[100];
  int numero;
};
